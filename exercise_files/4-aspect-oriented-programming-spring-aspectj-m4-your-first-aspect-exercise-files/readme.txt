To run the demos you should install the Spring Tool Suite from http://www.springsource.org/sts . It is based on Eclipse and runs on Windows, Linux and Mac OS X.

To import the demos:
- Go to the "File" menu
- Choose "Import..."
- A dialog appears
- In "General" find "Existing Projects into Workspace"
- Click on the button "Next"
- The next page of the dialog appears
- Choose the option "Select archive file"
- Press the button "Browse..." right of "Select archive file"
- Find the demo.zip you want to use
- In "Projects" the new projects should be listed
- Press the "Finish" button
- Enjoy!
